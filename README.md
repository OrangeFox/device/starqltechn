# Device Tree for Samsung Galaxy S9 (SM-G9600)
Device Tree Made by Klabit and edited by Renatoh Ribeiro
## Specs

|        Component        |          Specification            |
| :---------------------- | :-------------------------------- |
| Chipset                 | Qualcomm SDM845 Snapdragon 845    |
| Memory                  | 4 GB                              |
| Storage                 | 128 GB or 64GB                    |
| Battery                 | 3000 mAh (non-removable)          |
| Dimensions              | 147.7 x 68.7 x 8.5 m              |
| Display                 | 1440 x 2960 pixels, 18.5:9, 570PPI|
| Release Date            | 2018 March                        |

## Device Picture

![Galaxy S9](https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-s9-.jpg "Galaxy S9")
