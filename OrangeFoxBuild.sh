#!/bin/bash

# configure some default settings for the build
Default_Settings() {
	export ALLOW_MISSING_DEPENDENCIES=true
	export TARGET_ARCH="arm64"
	export TW_DEFAULT_LANGUAGE="en"
    export FOX_VERSION="R11.0"
    export FOX_R11="1"
    export TARGET_ARCH=arm64
    export OF_USE_MAGISKBOOT=1
    export FOX_VERSION=R11.0
    export OF_USE_MAGISKBOOT_FOR_ALL_PATCHES=1
    export OF_FLASHLIGHT_ENABLE=0
    export OF_SCREEN_H=2220
    export OF_DISABLE_MIUI_SPECIFIC_FEATURES=1
    export FOX_DELETE_AROMAFM=1
    export OF_USE_TWRP_SAR_DETECT=1
    export FOX_BUILD_TYPE="Stable"
    export OF_MAINTAINER="Renatoh"
    export FOX_R11=1
    export FOX_RECOVERY_INSTALL_PARTITION="/dev/block/platform/soc/1d84000.ufshc/by-name/recovery"
    export FOX_ADVANCED_SECURITY=1
    export OF_USE_TWRP_SAR_DETECT=1
}

# build the project
do_build() {
  Default_Settings

  # compile it
  . build/envsetup.sh
  
  lunch omni_starqltechn-eng
  
  mka recoveryimage -j`nproc`
}

# --- main --- #
do_build
#